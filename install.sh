#!/bin/bash

if [ -f /etc/os-release ]; then
    platform=$(cat /etc/os-release | grep -v VERSION | grep "ID\=" | sed -e s/'ID='/''/ | sed -e s/'\"'/''/g)
    version=$(cat /etc/os-release | grep "VERSION_ID" | sed -e s/'VERSION_ID='/''/ | sed -e s/'\"'/''/g)
else
    platform="centos"
    version="6"
fi

echo "Platform: $platform"
echo "Version: $version"

hostname=$(hostname)

case $platform in
    "centos")
        if [ "$version" == "7" ]; then
            rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-2.el7.noarch.rpm
            yum -y install zabbix-agent
            systemctl enable zabbix-agent
        else
            rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/6/x86_64/zabbix-release-4.0-2.el6.noarch.rpm
            yum -y install zabbix-agent
            chkconfig on zabbix-agent
        fi
    ;;
    "ubuntu")
        if [ "$version" == "18.04" ]; then
            wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+bionic_all.deb -O /tmp/zabbix-agent-4.0.3.deb
            dpkg -i /tmp/zabbix-agent-4.0.3.deb
            apt-get update && apt-get install -y zabbix-agent
        elif [ "$version" == "16.04" ]; then
            wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+xenial_all.deb -O /tmp/zabbix-agent-4.0.3.deb
            dpkg -i /tmp/zabbix-agent-4.0.3.deb
            apt-get update && apt-get install -y zabbix-agent
        elif [ "$version" == "20.04" ]; then
            wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb -O /tmp/zabbix-agent-5.0.1.deb
            dpkg -i /tmp/zabbix-agent-5.0.1.deb
            apt-get update && apt-get install -y zabbix-agent
        fi
        systemctl enable zabbix-agent
    ;;
esac

cp /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf-dist

mkdir -p /etc/zabbix/zabbix_agentd.d
mkdir -p /var/run/zabbix
mkdir -p /var/log/zabbix

cat << EOF > /etc/zabbix/zabbix_agentd.conf
# The location of the Zabbix Agent PID file
PidFile=/var/run/zabbix/zabbix_agentd.pid 
# The location of the Zabbix Agent log files
LogFile=/var/log/zabbix/zabbix_agentd.log 
# How large (in MB) should the log file grow until Zabbix rotates it
LogFileSize=25
# The IP address of the Zabbix Server or Zabbix Proxy
Server=207.166.186.110
ServerActive=207.166.186.110
# The Hostname.  This must match the hostname added to the Zabbix Server
Hostname=$hostname
# include additional configuration files
Include=/etc/zabbix/zabbix_agentd.d/
EOF

iptables -I INPUT -s 207.166.186.110 -p tcp -m tcp --dport 10050 -j ACCEPT
service iptables save

if [ "$platform" == "centos" ]; then
    if [ "$version" == "7" ]; then
        systemctl start zabbix-agent
    else
        service zabbix-agent start
    fi
else
    systemctl start zabbix-agent
fi